<?php

use App\Http\Controllers\LineController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Login API
Route::get('/', function () {
    return view('line_login');
})->name('login');

Route::get('/line/login', [LineController::class, 'lineLogin'])->name('line_login');
Route::get('/line/login/callback', [LineController::class, 'lineDetail'])->name('line_detail');

// Message API
Route::match(['get', 'post'], '/webhook', [LineController::class, 'webhookMessage'])->name('line_messaging_api_webhook');

Route::get('/shop/{shop_id}', [LineController::class, 'lineShopLogin'])->name('line_shop_login');

// Liff
Route::get('/liff', function () {
    return view('liff');
});

Route::get('/liff-v2', function () {
    return view('liff-v2');
});
