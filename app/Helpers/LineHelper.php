<?php

namespace App\Helpers;

use LINE\LINEBot;
use LINE\LINEBot\HTTPClient\CurlHTTPClient;

class LineHelper extends LINEBot
{
    public function __construct(array $args = [])
    {
        $httpClient = new CurlHTTPClient(env('MESSAGE_CHANNEL_ACCESS_TOKEN'));
        parent::__construct($httpClient, [$args, 'channelSecret' => env('MESSAGE_CHANNEL_SECRET')]);
    }
}
