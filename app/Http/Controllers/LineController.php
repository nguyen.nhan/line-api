<?php

namespace App\Http\Controllers;

use App\Helpers\LineHelper;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use LINE\LINEBot\MessageBuilder\TextMessageBuilder;

class LineController extends Controller
{
    const AUTHOR_REQUEST_URL = 'https://access.line.me/oauth2/v2.1/authorize';
    const RESPONSE_TYPE = 'code';
    const STATE = 'random_string';
    const SCOPE = 'profile%20openid%20email';

    const DATA_LOGIN = [
        16820 => [
            'LOGIN_CHANNEL_ID'             => '1656482688',
            'LOGIN_CHANNEL_SECRET'         => '6fa3ebb901d8ea10fc0de859e9015d68',
            'MESSAGE_CHANNEL_SECRET'       => 'ff141d1438211c7f0ce373eb2fd7949d',
            'MESSAGE_CHANNEL_ACCESS_TOKEN' => 'WBNI3pRzXENzKs3XHIc97TGAmMj2Kc8bUNfOe9INhFWOg4KpT19XIRtbprbctNHVnKAuqJCUXJlwBkRjZE/QxdHuUKZ58LmZ8nvNhVoNNyOfmrZxgmuYqgx8f4zIT9sQrruD1c6eu/0kSySXBnXHSAdB04t89/1O/w1cDnyilFU=',
        ],
        4842 => [
            'LOGIN_CHANNEL_ID'             => '1656467746',
            'LOGIN_CHANNEL_SECRET'         => 'aab8029b2c27cd820a769e5e78edb24c',
            'MESSAGE_CHANNEL_SECRET'       => '8a55c57b595b999181de872f555c8326',
            'MESSAGE_CHANNEL_ACCESS_TOKEN' => 'VRnFb40z0Ga6Y0meft9B/LrzU+pNjWNNJYPv3bgscZgAkS/IxdgBZ5RBD/RmQXZAhIFc7kLtfn6r3+z5PaBVHioCZ3xVqYpu3OEW0y7k1O/8EZjSkFosWaayhvXU66PiQNa5ZmPMpAifkOT5t8PNsgdB04t89/1O/w1cDnyilFU=',
        ],
    ];

    private $lineHelper;

    private $data_token = [
        'LOGIN_CHANNEL_ID'             => '',
        'LOGIN_CHANNEL_SECRET'         => '',
        'MESSAGE_CHANNEL_SECRET'       => '',
        'MESSAGE_CHANNEL_ACCESS_TOKEN' => '',
    ];

    private $params = [];

    public function __construct(Request $request)
    {
        if ($request->get('state')) {
            $state        = base64_decode($request->state);
            $this->params = json_decode($state, true);
            if (!empty($this->params['shop_id'])) {
                $request->request->add(['shop_id' => $this->params['shop_id']]);
            }
        }

        if ($request->get('shop_id') && !empty(self::DATA_LOGIN[$request->get('shop_id')])) {
            $token_info = self::DATA_LOGIN[$request->get('shop_id')];

            $this->data_token['LOGIN_CHANNEL_ID']             = $token_info['LOGIN_CHANNEL_ID'];
            $this->data_token['LOGIN_CHANNEL_SECRET']         = $token_info['LOGIN_CHANNEL_SECRET'];
            $this->data_token['MESSAGE_CHANNEL_ACCESS_TOKEN'] = $token_info['MESSAGE_CHANNEL_ACCESS_TOKEN'];
            $this->data_token['MESSAGE_CHANNEL_SECRET']       = $token_info['MESSAGE_CHANNEL_SECRET'];
        } else {
            $this->data_token['LOGIN_CHANNEL_ID']             = env('LOGIN_CHANNEL_ID');
            $this->data_token['LOGIN_CHANNEL_SECRET']         = env('LOGIN_CHANNEL_SECRET');
            $this->data_token['MESSAGE_CHANNEL_ACCESS_TOKEN'] = env('MESSAGE_CHANNEL_ACCESS_TOKEN');
            $this->data_token['MESSAGE_CHANNEL_SECRET']       = env('MESSAGE_CHANNEL_SECRET');
        }

        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($this->data_token['MESSAGE_CHANNEL_ACCESS_TOKEN']);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $this->data_token['MESSAGE_CHANNEL_SECRET']]);

        $this->lineHelper = $bot;
    }

    public function lineLogin(Request $request)
    {
        $data            = [];
        $data['text']    = $request->text;
        $data['shop_id'] = $request->shop_id;
        $data['time']    = Carbon::now()->getTimestamp();

        $data  = json_encode($data);
        $state = base64_encode($data);

        $params = [
            'response_type' => self::RESPONSE_TYPE,
            'client_id'     => $this->data_token['LOGIN_CHANNEL_ID'],
            'redirect_uri'  => route('line_detail'),
            'state'         => $state,
            'scope'         => 'chat_message.write%20openid%20profile%20real_name%20gender%20birthdate%20phone%20address%20email',
            'disable_auto_login' => 'true',
            'bot_prompt' => 'normal',
        ];

        $url = self::AUTHOR_REQUEST_URL . '?' . http_build_query($params);
        $url = urldecode($url);

        return redirect($url);
    }

    public function lineShopLogin($shop_id) {
        return view('line_login', compact(['shop_id']));
    }

    public function lineDetail(Request $request)
    {
        $code = $request->code;
        $data = $request->toArray();

        try {
            $getAccessToken = $this->getAccessToken($code);
            $getUserProfile = $this->getUserProfile($getAccessToken->access_token);

            if (!empty($this->params['text'])) {
                $this->pushMessageToUser($getUserProfile->userId, $this->params['text']);
            }

        } catch (\Throwable $th) {
            return redirect()->route('login');
        }
        return view('line_detail', compact(['code', 'data', 'getUserProfile', 'getAccessToken']));
    }

    private function pushMessageToUser($userID, $mgs) {
        $message   = [];
        $message[] = [
            'type' => 'text',
            'text' => $mgs,
        ];

        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $this->data_token['MESSAGE_CHANNEL_ACCESS_TOKEN'],
            'Accept'        => 'application/json',
        ];

        $client->post('https://api.line.me/v2/bot/message/push', [
            'headers' => $headers,
            'json' => [
                'to' => $userID,
                'messages' => $message,
            ]
        ]);

        return true;
    }

    private function getAccessToken($code)
    {
        $client = new Client();
        $response = $client->post('https://api.line.me/oauth2/v2.1/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'code' => $code,
                'redirect_uri' => route('line_detail'),
                'client_id' => $this->data_token['LOGIN_CHANNEL_ID'],
                'client_secret' => $this->data_token['LOGIN_CHANNEL_SECRET']
            ]
        ]);

        return json_decode($response->getBody()->getContents(), false);
    }

    public function getUserProfile($access_token)
    {
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $access_token,
            'Accept'        => 'application/json',
        ];

        $response = $client->get('https://api.line.me/v2/profile', [
            'headers' => $headers
        ]);

        return json_decode($response->getBody()->getContents(), false);
    }

    public function curlSendMessage($replyJson, $token)
    {
        $ch = curl_init($token["URL"]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Authorization: Bearer ' . $token["AccessToken"]
            )
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $replyJson);
        $result = curl_exec($ch);
        curl_close($ch);

        Log::channel('daily')->info($result);

        return $result;
    }

    public function webhookMessage(Request $request, $shop_id = 0)
    {
        Log::channel('daily')->info($request->all());
        if ($request['events'][0]['type'] === 'message') {
            $message = '{
                "type" : "text",
                "text" : "Hello My Friend"
                }';
            $replymessage = json_decode($message);

            $lineData['URL'] = "https://api.line.me/v2/bot/message/reply";
            $lineData['AccessToken'] = $this->data_token['MESSAGE_CHANNEL_ACCESS_TOKEN'];
            $replyJson["replyToken"] = $request['events'][0]['replyToken'];
            $replyJson["messages"][0] = $replymessage;

            $encodeJson = json_encode($replyJson);

            $results = $this->curlSendMessage($encodeJson, $lineData);

            // Use Plugin
            Log::channel('daily')->info($request['events'][0]['replyToken']);
            $result = $this->lineHelper->replyText($request['events'][0]['replyToken'], 'From LINEBot with love!');
            if (!$result->isSucceeded()) {
                Log::channel('daily')->info($result->getHTTPStatus() . ' ' . $result->getRawBody());
            }

            $groupId = 'Cecc3c34ffea51d1ab15de2342ced7935';
            $message = new TextMessageBuilder('This is Push Message to Test GR');
            $pushMessageGr = $this->lineHelper->pushMessage($groupId, $message);
            if (!$pushMessageGr->isSucceeded()) {
                Log::channel('daily')->info('This is Push Message to Test GR' . $pushMessageGr->getHTTPStatus() . ' ' . $pushMessageGr->getRawBody());
            }

            $userId = $request['events'][0]['source']['userId'];
            $user = $this->lineHelper->getProfile($userId);

            Log::channel('daily')->info('getProfile');
            Log::channel('daily')->info($user->getJSONDecodedBody());
            $message = new TextMessageBuilder('This is Push Message to ' . $user->getJSONDecodedBody()['displayName']);
            $pushMessageUser = $this->lineHelper->pushMessage($userId, $message);
            if (!$pushMessageUser->isSucceeded()) {
                Log::channel('daily')->info('This is Push Message to ' . $user->getJSONDecodedBody()['displayName'] . $pushMessageUser->getHTTPStatus() . ' ' . $pushMessageUser->getRawBody());
            }

            $message = new TextMessageBuilder('userID ' . $user->getJSONDecodedBody()['displayName'] . ': ' . $userId);
            $pushMessageUser = $this->lineHelper->pushMessage($userId, $message);

            // link token
            $response = $this->lineHelper->createLinkToken($userId);
            Log::channel('daily')->info($response->getJSONDecodedBody());
            $linkToken = $response->getJSONDecodedBody()['linkToken'];

            $broadcastMessage = $this->lineHelper->broadcast(new TextMessageBuilder('This is Broadcast message from Bot'));
            if (!$broadcastMessage->isSucceeded()) {
                Log::channel('daily')->info('Broadcast message:' . $broadcastMessage->getHTTPStatus() . ' ' . $broadcastMessage->getRawBody());
            }
        }
    }
}
