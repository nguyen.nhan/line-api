<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liff App</title>
</head>

<body>
    <button id="open">Open Window</button>
    <section id="profile">
        <img id="pictureUrl" src="https://mokmoon.com/images/ic_liff.png">
        <p id="userId"></p>
        <p id="displayName"></p>
        <p id="statusMessage"></p>
        <p id="email"></p>
    </section>
    <section id="button">
        <button id="btnSend">Send Message</button>
    <!-- ... -->
    </section>
    <script charset="utf-8" src="https://static.line-scdn.net/liff/edge/2/sdk.js"></script>
    <script>
        main();
        async function main () {
            // Initialize LIFF app
            await liff.init({ liffId: "1656471552-D18Gevl1" });
            if (liff.isLoggedIn()) {
                getUserProfile();
            } else {
                liff.login();
            }
        }

        async function getUserProfile() {
            const profile = await liff.getProfile()
            console.log(profile);

            pictureUrl.src = profile.pictureUrl
            userId.innerHTML = "<b>userId:</b> " + profile.userId
            statusMessage.innerHTML = "<b>statusMessage:</b> " + profile.statusMessage
            displayName.innerHTML = "<b>displayName:</b> " + profile.displayName
            email.innerHTML = "<b>email:</b> " + liff.getDecodedIDToken().email
        }

        async function sendMsg() {
            await liff.sendMessages([{
                type: 'text',
                text: "I use liff line"
            }]).then(function () {
                window.alert("Message sent");
            }).catch(function (error) {
                window.alert("Error sending message: " + error);
            });
        }

        btnSend.onclick = () => {
            sendMsg()
        }

        window.onload = function(e){
            document.getElementById('open').addEventListener('click', function() {
                liff.openWindow({
                    url: 'https://line.me',
                    external: true
                });

                console.log(53543);
            });
        }
    </script>
</body>

</html>
